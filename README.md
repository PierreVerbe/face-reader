# Face reader

## About this repository
This repository concerns our "Face reader" project which is an end of semester project for the machine learning course given at ISEP. <br>
We have developed a CNN that predicts according to a face its gender and age. <br>

## Prerequisite
* Install Python 
* An IDE for example PyCharm, VsCode, ...

## Installation
* First clone this project
```bash
git clone https://gitlab.com/PierreVerbe/face-reader
```
* Download ZIP file["Aligned & Cropped Faces"](https://susanqq.github.io/UTKFace/)
* Unzip it and put it in the folder resources/
* Install the following python packages
```bash
pip install opencv-python tensorflow keras sklearn matplotlib cvlib runipy
```

## Launching the model training
* Run this file
```bash
runipy -o .\faceReader\Face_Reader_Train.ipynb
```

## Launching the demonstration
* Run this file
```bash
runipy -o .\faceReader\Face_Reader_Webcam.ipynb
```

## Notes
* Our dataset <br>
    https://susanqq.github.io/UTKFace/
