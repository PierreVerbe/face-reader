# Face reader

## Prerequisites
* Python
* An IDE for example PyCharm, VsCode, ...

## Installation 
* Download ZIP file["Aligned & Cropped Faces"](https://susanqq.github.io/UTKFace/)
* Unzip it and put it in the folder resources/
* Install the following python packages
    ```
    pip install opencv-python tensorflow keras sklearn matplotlib cvlib
    ```

## Run the application

## Author
* Pierre Verbe → @PierreVerbe

https://towardsdatascience.com/wtf-is-image-classification-8e78a8235acb
https://towardsdatascience.com/facial-data-based-deep-learning-emotion-age-and-gender-prediction-47f2cc1edda7